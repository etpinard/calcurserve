# Calcurserve

[Calcurse](https://www.calcurse.org/) calendar app with git-tracked,
available-via-ssh data and [signal-cli](https://github.com/AsamK/signal-cli)
notifications.

## Server-side setup

The server will host a git repo of your calcurse calendar data and trigger
Signal notifications via crontabs.

Prerequisites on the server:

1) Install git, calcurse and signal-cli,
1) Make the data server is available via ssh,
1) Get a second phone number to have a separate Signal user for the host[^1],
1) Register that second phone number with signal-cli,
   see instructions [here](https://github.com/AsamK/signal-cli?tab=readme-ov-file#usage).

Then,

```sh
git clone git@gitlab.com:etpinard/calcurserve.git
cd calcurserve
./install-server.sh
```

## Client-side setup

The client fetches new calcurse data (if any), display the calcurse data in
calcurse and push new calcurse data (if any) back to the host.

Prerequisites on client:

1) Install git and calcurse,
1) Make sure the data server is accessible via ssh from the client.

Then,

```sh
git clone git@gitlab.com:etpinard/calcurserve.git
cd calcurserve
./install-client.sh
```

Open with:

```sh
calcurserve
```

Note: the steps above should just work in
[Termux](https://f-droid.org/packages/com.termux/) as well, bringing Calcurserve
to your Android-based phone.

Note: I chose to not use calcurse _hooks_ and write a wrapper script
`calcurserve` script instead for flexibility to support Termux.

## License

[MIT](./LICENSE) - etpinard (2024)

## Footnotes

[^1]: There's probably a way to make signal-cli send notifications via "Note to
    Self" instead which would make calcurserve work with a single phone number,
    but this is not implemented (yet). Contributions are welcome!
