#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

# shellcheck disable=SC1091
source "$HERE/lib/paths.sh"

_maybe_configure_vars () {
    if [ -f "$VARS_CLIENT" ]; then
        echo 'INFO: client.sh config file found!'
    else
        read -rp "Enter ssh host name of calcurse data server: " \
            CALCURSERVE_SSH_HOST
        read -rp "Enter path on server to calcurse data directory (must be same as for server) " \
            CALCURSERVE_DATA_DIR_SERVER
        read -rp "Enter path to calcurse config directory [~/.config/calcurse]: " \
            CALCURSE_CONFIG_DIR
        CALCURSE_CONFIG_DIR="${CALCURSE_CONFIG_DIR:-$HOME/.config/calcurse}"
        read -rp "Enter path to calcurse data directory [~/.local/share/calcurse]: " \
            CALCURSE_DATA_DIR
        CALCURSE_DATA_DIR="${CALCURSE_DATA_DIR:-$HOME/.local/share/calcurse}"
        read -rp "Enter path calcurserve executable [~/bin/calcuserve]: " \
            CALCURSERVE_BIN_PATH
        CALCURSERVE_BIN_PATH="${CALCURSERVE_BIN_PATH:-$HOME/bin/calcurserve}"

        mkdir -p "$CALCURSERVE_CONFIG_DIR"
        { echo "export CALCURSERVE_SSH_HOST=$CALCURSERVE_SSH_HOST";
          echo "export CALCURSERVE_DATA_DIR_SERVER=$CALCURSERVE_DATA_DIR_SERVER";
          echo "export CALCURSE_CONFIG_DIR=$CALCURSE_CONFIG_DIR";
          echo "export CALCURSE_DATA_DIR=$CALCURSE_DATA_DIR";
          echo "export CALCURSERVE_BIN_PATH=$CALCURSERVE_BIN_PATH"; } > "$VARS_CLIENT"

        if "$HERE/lib/ami-on-termux.sh"; then
            read -rp "Enter path to ssh key associated with calcurse data server: " \
                CALCURSERVE_SSH_KEY_PATH
            echo "export CALCURSERVE_SSH_KEY_PATH=$CALCURSERVE_SSH_KEY_PATH" >> "$VARS_CLIENT"
        fi

        echo "INFO: wrote client.sh config file at $VARS_CLIENT"
    fi
}

_validate_vars () {
    if [ -z ${CALCURSERVE_SSH_HOST+x} ]; then
        echo "ERR: CALCURSERVE_SSH_HOST is not set, exit" >&2
        exit 1
    fi
    if [ -z ${CALCURSERVE_DATA_DIR_SERVER+x} ]; then
        echo "ERR: CALCURSERVE_DATA_DIR_SERVER is not set, exit" >&2
        exit 1
    fi
    if [ -z ${CALCURSERVE_CONFIG_DIR+x} ]; then
        echo "ERR: CALCURSERVE_CONFIG_DIR is not set, exit" >&2
        exit 1
    fi
    if [ -z ${CALCURSE_DATA_DIR+x} ]; then
        echo "ERR: CALCURSE_DATA_DIR is not set, exit" >&2
        exit 1
    fi
    if [ -z ${CALCURSERVE_BIN_PATH+x} ]; then
        echo "ERR: CALCURSERVE_BIN_PATH is not set, exit" >&2
        exit 1
    fi
    if "$HERE/lib/ami-on-termux.sh"; then
        if [ -z ${CALCURSERVE_SSH_KEY_PATH+x} ]; then
            echo "ERR: CALCURSERVE_SSH_KEY_PATH is not set, exit" >&2
            exit 1
        fi
    fi
}

_are_deps_installed () {
    if ! [ -x "$(command -v calcurse)" ]; then
      echo 'ERR: calcurse is not installed, exit' >&2
      exit 1
    fi
    if ! [ -x "$(command -v git)" ]; then
      echo 'ERR: git is not installed, exit' >&2
      exit 1
    fi
}

_link_conf () {
    mkdir -p "$CALCURSE_CONFIG_DIR"
    pushd "$CALCURSE_CONFIG_DIR" > /dev/null
    echo 'INFO: link calcurse.conf'
    ln -fs "$HERE/lib/calcurse.conf" conf
    popd > /dev/null
}

_link_calcurserve_script () {
    _parentdir="$(dirname "$CALCURSERVE_BIN_PATH")"
    _leaf="$(basename "$CALCURSERVE_BIN_PATH")"
    mkdir -p "$_parentdir"
    pushd "$_parentdir" > /dev/null
    echo 'INFO: link calcurserve script'
    ln -fs "$HERE/calcurserve" "$_leaf"
    popd > /dev/null
}

_clone_calcurse_data () {
    local dir="$CALCURSE_DATA_DIR"
    _parentdir="$(dirname "$dir")"
    _leaf="$(basename "$dir")"
    mkdir -p "$_parentdir"

    echo "INFO: cloning repo $_leaf from $CALCURSERVE_SSH_HOST"
    pushd "$_parentdir" > /dev/null
    git clone "$CALCURSERVE_SSH_HOST":"$CALCURSERVE_DATA_DIR_SERVER" "$_leaf"
    popd > /dev/null
}

_maybe_clone_calcurse_data () {
    local dir="$CALCURSE_DATA_DIR"

    if [ -d "$dir" ]; then
        read -r -p "$dir already exist, remove? [y/N] " response
        case "$response" in
            [Yy]*)
                echo "INFO: removing $dir"
                rm -rf "$dir"
                _clone_calcurse_data
                ;;
            *)
                echo "INFO: skip cloning calcurse data"
                ;;
        esac
    else
        _clone_calcurse_data
    fi
}

main () {
    _maybe_configure_vars
    # shellcheck disable=SC1090
    source "$VARS_CLIENT"
    _validate_vars
    _are_deps_installed
    _link_conf
    _link_calcurserve_script
    _maybe_clone_calcurse_data
}

main
