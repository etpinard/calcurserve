#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

# shellcheck disable=SC1091
source "$HERE/lib/paths.sh"

_maybe_configure_vars () {
    if [ -f "$VARS_SERVER" ]; then
        echo 'INFO: server.sh config file found!'
    else
        read -rp "Enter path on server to calcurse data directory (must be same as for client): " \
            CALCURSERVE_DATA_DIR_SERVER
        read -rp "Enter signal phone number for server: " \
            CALCURSERVE_SIGNAL_NUMBER_SERVER
        read -rp "Enter signal phone number for client: " \
            CALCURSERVE_SIGNAL_NUMBER_CLIENT
        read -rp "Enter path to calcurserve logs directory [~/.local/share/calcurserve/log]: " \
            CALCURSERVE_LOGS_DIR
        CALCURSERVE_LOGS_DIR="${CALCURSERVE_LOGS_DIR:-$HOME/.local/share/calcurserve/log}"

        mkdir -p "$CALCURSERVE_CONFIG_DIR"
        { echo "export CALCURSERVE_DATA_DIR_SERVER=$CALCURSERVE_DATA_DIR_SERVER";
          echo "export CALCURSERVE_SIGNAL_NUMBER_SERVER=$CALCURSERVE_SIGNAL_NUMBER_SERVER";
          echo "export CALCURSERVE_SIGNAL_NUMBER_CLIENT=$CALCURSERVE_SIGNAL_NUMBER_CLIENT";
          echo "export CALCURSERVE_LOGS_DIR=$CALCURSERVE_LOGS_DIR"; } > "$VARS_SERVER"

        echo "INFO: wrote client.sh config file at $VARS_CLIENT"
    fi
}

_validate_vars () {
    if [ -z ${CALCURSERVE_DATA_DIR_SERVER+x} ]; then
        echo "ERR: CALCURSERVE_DATA_DIR_SERVER is not set, exit" >&2
        exit 1
    fi
    if [ -z ${CALCURSERVE_SIGNAL_NUMBER_SERVER+x} ]; then
        echo "ERR: CALCURSERVE_SIGNAL_NUMBER_SERVER is not set, exit" >&2
        exit 1
    fi
    if [ -z ${CALCURSERVE_SIGNAL_NUMBER_CLIENT+x} ]; then
        echo "ERR: CALCURSERVE_SIGNAL_NUMBER_CLIENT is not set, exit" >&2
        exit 1
    fi
    if [ -z ${CALCURSERVE_LOGS_DIR+x} ]; then
        echo "ERR: CALCURSERVE_LOGS_DIR is not set, exit" >&2
        exit 1
    fi
}

LOG="$CALCURSERVE_LOGS_DIR/install-server.log"

_are_deps_installed () {
    if ! [ -x "$(command -v calcurse)" ]; then
      echo 'ERR: calcurse is not in PATH, exit' >&2
      exit 1
    fi
    if ! [ -x "$(command -v git)" ]; then
      echo 'ERR: git is not in PATH, exit' >&2
      exit 1
    fi
    if ! [ -x "$(command -v signal-cli)" ]; then
      echo 'ERR: signal-cli is not in path, exit' >&2
      exit 1
    fi
}

_test_send_signal_msg () {
    echo 'INFO: test sending message via signal-cli'
    signal-cli -u "$CALCURSERVE_SIGNAL_NUMBER_SERVER" \
        send -m "test for the calcurserve install-server.sh script" \
        "$CALCURSERVE_SIGNAL_NUMBER_CLIENT" \
        2>> "$LOG"
}

_init_calcurse_data_dir () {
    local _dir="$CALCURSERVE_DATA_DIR_SERVER"
    if [ -d "$_dir" ]; then
        echo "INFO: $_dir already exists, skip"
    else
        echo 'INFO: create calcurse data directory origin'
        mkdir -p "$_dir"
        pushd "$_dir" > /dev/null
        git init --bare
        popd > /dev/null
    fi
}

# solution taking from:
# <https://stackoverflow.com/a/24892547>
_add_crontabs () {
    echo 'INFO: add crontabs'
    local daily="6 12 * * * $HERE/lib/cron-daily.sh"
    (crontab -l ; echo "$daily") 2>&1 | grep -v "no crontab" | sort | uniq | crontab -
}

main () {
    _maybe_configure_vars
    # shellcheck disable=SC1090
    source "$VARS_SERVER"
    _validate_vars
    _are_deps_installed
    _test_send_signal_msg
    _init_calcurse_data_dir
    _add_crontabs
}

main
