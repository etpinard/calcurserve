#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

NOW="$(date +"%Y-%m-%dT%H:%M:%S")"

# shellcheck disable=SC1091
source "$HERE/paths.sh"

# shellcheck disable=SC1090
source "$VARS_SERVER"

LOG="$CALCURSERVE_LOGS_DIR/cron-daily.log"

_receive_msgs () {
    # N.B. we need to do before sending, if not `signal-cli send` will fail
    signal-cli -u "$CALCURSERVE_SIGNAL_NUMBER_SERVER" receive
}

_maybe_send_notif () {
    pushd "$CALCURSERVE_DATA_DIR_SERVER" > /dev/null || exit

    git pull --force
    msg=$(calcurse -a)
    echo "$NOW" >> "$LOG"

    if [[ -z "$msg" ]]; then
        echo "no events today" >> "$LOG"
    else
        signal-cli -u "$CALCURSERVE_SIGNAL_NUMBER_SERVER" \
            send -m "\"$msg\"" \
            "$CALCURSERVE_SIGNAL_NUMBER_CLIENT" \
            2>> "$LOG"
        echo "$msg" >> "$LOG"
    fi

    popd > /dev/null || exit
}

main () {
    _receive_msgs
    _maybe_send_notif
}

main
