#! /bin/bash -e

EXIT_CODE=1

main () {
    cat /etc/hostname 2>/dev/null 1>/dev/null || EXIT_CODE=0
}

main

exit "$EXIT_CODE"
