#! /bin/bash -e

# shellcheck disable=SC2034
CALCURSERVE_CONFIG_DIR="${CALCURSERVE_CONFIG_DIR:-$HOME/.config/calcurserve}"

# shellcheck disable=SC2034
VARS_CLIENT="$CALCURSERVE_CONFIG_DIR/client.sh"

# shellcheck disable=SC2034
VARS_SERVER="$CALCURSERVE_CONFIG_DIR/server.sh"
