#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

# shellcheck disable=SC1091
source "$HERE/paths.sh"

# shellcheck disable=SC1090
source "$VARS_CLIENT"

_check_for_stashed_branches () {
    echo 'TODO'
}

_pull_modifs () {
    local main="master" # TODO generalise!
    git checkout "$main"
    git pull --no-rebase --force || true
}

main () {
    pushd "$CALCURSE_DATA_DIR" > /dev/null || exit

    if "$HERE/check-connection.sh"; then
        _pull_modifs
    else
        echo "WARN: could not connect to $CALCURSERVE_SSH_HOST"
        echo "WARN: this may not be the most up-to-date version"
    fi

    popd > /dev/null || exit
}

main
