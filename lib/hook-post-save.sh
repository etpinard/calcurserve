#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

NOW="$(date +"%Y-%m-%dT%H:%M:%S")"

# shellcheck disable=SC1091
source "$HERE/paths.sh"

# shellcheck disable=SC1090
source "$VARS_CLIENT"

_commit_msg () {
    echo "modif from $("$HERE/get-hostname.sh") on $NOW"
}

_maybe_commit () {
    git add --all
    if git diff --cached --exit-code; then
        echo "INFO: nothing to commit, skip"
    else
        git commit -m "$(_commit_msg)"
    fi
}

_push_modifs () {
    _maybe_commit
    git push
}

_stash_modifs () {
    local branch="unpush-modif-$NOW"
    git checkout --branch "$branch"
    _maybe_commit
    echo "INFO: modifs saved off $branch"
}

main () {
    pushd "$CALCURSE_DATA_DIR" > /dev/null || exit

    if "$HERE/check-connection.sh"; then
        _push_modifs
    else
        echo "WARN: could not connect to $CALCURSERVE_SSH_HOST"
        _stash_modifs
    fi

    popd > /dev/null || exit
}

main
