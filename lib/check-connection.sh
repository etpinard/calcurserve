#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

# shellcheck disable=SC1091
source "$HERE/paths.sh"

# shellcheck disable=SC1090
source "$VARS_CLIENT"

EXIT_CODE=0

check () {
    ssh -q "$CALCURSERVE_SSH_HOST" exit
}

main () {
    if check; then
        echo "INFO: connection to $CALCURSERVE_SSH_HOST successful"
    else
        echo "ERR: could not connect to $CALCURSERVE_SSH_HOST"
        echo "     this may not be the most up-to-date version"
        EXIT_CODE=1
    fi
}

main

exit "$EXIT_CODE"
