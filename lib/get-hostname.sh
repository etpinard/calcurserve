#! /bin/bash -e

HERE=$(dirname "$(readlink -f "$0")")

main () {
    if "$HERE/ami-on-termux.sh"; then
        echo 'termux'
        # TODO could do better with:
        # <https://github.com/termux/termux-api/issues/448>
    else
        hostname
    fi
}

main
